from setuptools import setup, find_packages
setup(
    name="requestslite",
    version="0.1.0",
    description="interface for http library",
    long_description="interface for making http python library easier to work with",
    author='cody johnson',
    author_email='codyj110@live.com',
    packages=find_packages(),
    url='https://gitlab.com/Codyj110/urllibHelper/wikis/home',
    license='GNU GPLv3',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Programming Language :: Python',
        'Topic :: Internet',
        'Topic :: Software Development'
    ]
)
