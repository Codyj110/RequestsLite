import urllib
import urllib.parse
import http.client
import base64
import json
import re
import xml.etree.ElementTree as XML
from collections import namedtuple
from enum import Enum


class requests:
    """A class to help make urllib and http.client easier to work with"""

    class contentType(Enum):
        xwwwformurlencoded = "application/x-www-form-urlencoded"
        json = "application/json"
        multipart = "multipart/form-data"

    def __init__(self):
        self.EncodedCredentials = None
        self.Token = None

    def PostRequest(self, url, contentType=None, headers=None, form=None, body=None, useBasicAuth=False,
                    useBearerToken=False, decode=False):
        """
        Deprecated
        makes post request with given params
        :param url:
        :param contentType:
        :param headers:
        :param form:
        :param body:
        :param useBasicAuth:
        :param useBearerToken:
        :param decode:
        :return:
        """

        if form is not None:
            payload = form
        else:
            payload = body

        return self.post_request(url, contentType, headers, payload, useBasicAuth, useBearerToken, decode)

    def post_request(self, url: str, content_type: str, headers: dict, payload: str, use_basic_auth: bool, use_bearer_token: bool, decode: bool):
        """
        makes post request with given params
        :param url:
        :param content_type:
        :param headers:
        :param payload:
        :param use_basic_auth:
        :param use_bearer_token:
        :param decode:
        :return:
        """
        headers = self.setup_headers(headers, content_type, use_basic_auth, use_bearer_token)

        split_url = self.seperate_url(url)

        response = self.make_request("POST", split_url.host, split_url.url, headers, payload)

        return self.decodeParse(response, decode)

    def GetRequest(self, url, contentType=None, headers={}, queryString=None, useBasicAuth=False,
                   useBearerToken=False, decode=False):
        """
        Deprecated
        make get request with given params
        :param url:
        :param contentType:
        :param headers:
        :param queryString:
        :param useBasicAuth:
        :param useBearerToken:
        :param decode:
        :return:
        """
        return self.get_request(url, contentType, headers, queryString, useBasicAuth, useBearerToken, decode)

    def get_request(self, url: str, content_type: str=None, headers: dict=None, query_string: dict=None,
                    use_basic_auth: bool=False, use_bearer_token: bool=False, decode=False):
        """
        makes get request with given params
        :param url:
        :param content_type:
        :param headers:
        :param query_string:
        :param use_basic_auth:
        :param use_bearer_token:
        :param decode:
        :return:
        """
        split_url = self.seperate_url(url, query_string)

        headers = self.setup_headers(headers, content_type, use_basic_auth, use_bearer_token)

        response = self.make_request('GET', split_url.host, split_url.url, headers, None)

        return self.decodeParse(response, decode)

    def setup_headers(self, headers: dict, content_type: str, use_basic_auth: bool, use_bearer_token: bool) -> dict:
        """
        setup headers for a request call
        :param headers:
        :param content_type:
        :param use_basic_auth:
        :param use_bearer_token:
        :return:
        """
        if headers is None:
            headers = {}

        headers = self.add_content_type_to_header(content_type, headers)
        self.addBasicAuthHttp(headers, use_basic_auth)
        self.addBearerTokenHttp(headers, use_bearer_token)

        return headers

    def make_request(self, request_type: str, host: str, url: str, headers: dict, payload: str=None):
        """
        make request call using http library
        :param request_type:
        :param host:
        :param url:
        :param headers:
        :param payload:
        :return:
        """
        connection = http.client.HTTPSConnection(host)
        connection.request(request_type, url, payload, headers)

        return connection.getresponse()

    def add_content_type_to_header(self, content_type: str, headers: dict) -> dict:
        """
        Add content_type header if present otherwise it will return original headers object
        :param content_type:
        :param headers:
        :return:
        """
        if content_type is not None:
            new_headers = headers
            new_headers['content-type'] = self.get_content_type(content_type)
            return new_headers
        else:
            return headers

    def get_content_type(self, type):
        """
        If user passes enum use that else use whatever they passed in
        :param type:
        :return:
        """
        try:
            return self.contentType[type].value
        except KeyError:
            return type

    def getBasicAuthCredentials(self):
        """
        decode binary credentials string and pass back
        :return:
        """
        return 'Basic %s' % self.EncodedCredentials.decode("ascii")

    def setBasicAuth(self, iD, secret):
        """
        formats id and secret in ascii binary string then base64 encodes
        :param iD:
        :param secret:
        :return:
        """
        credentials = ('%s:%s' % (iD, secret))
        self.EncodedCredentials = base64.b64encode(credentials.encode('ascii'))

    def setBearerToken(self, token):
        self.Token = token

    def addBasicAuthHttp(self, headers, useBasicAuth):
        if useBasicAuth:
            headers['authorization'] = self.getBasicAuthCredentials()

    def addBearerTokenHttp(self, headers, useBearerToken):
        if useBearerToken:
            headers['Authorization'] = 'Bearer %s' % self.Token

    def decodeParse(self, rawResponse, decode):
        if decode:
            return self.parseResponse(self.decodeResponse(self.read_response(rawResponse)))
        else:
            return rawResponse

    def read_response(self, rawResponse):
        return rawResponse.read()

    def seperate_url(self, url: str, query_string: dict=None) -> tuple:
        # seperate host and url for http standard lib
        pattern = '(?:http.*://)?(?P<host>[^:/ ]+)(?P<url>[/].+)'
        match = re.search(pattern, self.concat_query_string(url, query_string))
        url = namedtuple('url', 'host url')
        url_tuple = url(match.group('host'), match.group('url'))

        return url_tuple

    def concat_query_string(self, url: str, query_string: dict) -> str:
        new_url = url
        if query_string is not None:
            new_url += '?' + urllib.parse.urlencode(query_string)
        return new_url

    def decodeResponse(self, rawString):

        try:
            # attempt utf-8 first
            decodedString = rawString.decode('utf-8')

            # check to see if it is a signed utf-8 (utf-8-sig)
            if decodedString[0] == '\ufeff':
                decodedString = rawString.decode('utf-8-sig')

            return decodedString

        except UnicodeDecodeError as ex:
            # If unicode error try to decode as utf-16
            decodedString = rawString.decode('utf-16')
            return decodedString

    def parseResponse(self, response):
        if response[0] == '<':
            return XML.fromstring(response)
        else:
            return json.loads(response)




