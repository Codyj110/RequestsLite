from unittest.mock import patch
from requestslite.requests import requests
http = requests()

# to run code coverage: python3 -m pytest --cov-report html --cov=requestslite --cov-branch

def test_decode_utf_8_xml():
    # setup
    get_request_url = "https://my.test.url.com/endpoint"
    get_request_headers = {"x-api-key": "000b2bb0"}
    get_request_query_string = {"field1": "value1"}
    mock_return_value = b"<?xml version='1.0' encoding='UTF-8'?>\n<dataset>\n<record><id>1</id><name>Bigtax</name></record><record><id>2</id><name>Daltfresh</name></record></dataset>"
    expect = [{'id': '1', 'name': 'Bigtax'}, {'id': '2', 'name': 'Daltfresh'}]

    with patch.object(http, "make_request") as mock_make_request:
        with patch.object(requests, "read_response", return_value=mock_return_value) as mock_read:
            # act
            actual = http.GetRequest(get_request_url,
                            http.contentType.json.name,
                            get_request_headers,
                            get_request_query_string,
                            False,
                            False,
                            True)

            actual_object = []
            for id, name in actual.iter('record'):
                actual_object.append({'id':id.text, 'name':name.text})
            # assert
            assert actual_object == expect

def test_decode_utf_8():
    # setup
    get_request_url = "https://my.test.url.com/endpoint"
    get_request_headers = {"x-api-key": "000b2bb0"}
    get_request_query_string = {"field1": "value1"}
    mock_return_value = b'[{"myObject":{"id":1,"name":"wcrissil0"}},{"myObject":{"id":2,"name":"qabdee1"}},{"myObject":{"id":3,"name":"ckimpton2"}},{"myObject":{"id":4,"name":"cferran3"}},{"myObject":{"id":5,"name":"ililie4"}},{"myObject":{"id":6,"name":"wheineke5"}},{"myObject":{"id":7,"name":"kasprey6"}},{"myObject":{"id":8,"name":"eeuesden7"}},{"myObject":{"id":9,"name":"kpridmore8"}},{"myObject":{"id":10,"name":"akittiman9"}}]'
    expect = [{'myObject': {'id': 1, 'name': 'wcrissil0'}}, {'myObject': {'id': 2, 'name': 'qabdee1'}}, {'myObject': {'id': 3, 'name': 'ckimpton2'}}, {'myObject': {'id': 4, 'name': 'cferran3'}}, {'myObject': {'id': 5, 'name': 'ililie4'}}, {'myObject': {'id': 6, 'name': 'wheineke5'}}, {'myObject': {'id': 7, 'name': 'kasprey6'}}, {'myObject': {'id': 8, 'name': 'eeuesden7'}}, {'myObject': {'id': 9, 'name': 'kpridmore8'}}, {'myObject': {'id': 10, 'name': 'akittiman9'}}]

    with patch.object(http, "make_request") as mock_make_request:
        with patch.object(requests, "read_response", return_value=mock_return_value) as mock_read:
            # act
            actual = http.GetRequest(get_request_url,
                            http.contentType.json.name,
                            get_request_headers,
                            get_request_query_string,
                            False,
                            False,
                            True)
            # assert
            assert actual == expect

def test_decode_utf_8_sig():
    # setup
    get_request_url = "https://my.test.url.com/endpoint"
    get_request_headers = {"x-api-key": "000b2bb0"}
    get_request_query_string = {"field1": "value1"}
    mock_return_value = b'\xef\xbb\xbf[{"myObject": {"id": 1, "name": "wcrissil0"}}, {"myObject": {"id": 2, "name": "qabdee1"}}, {"myObject": {"id": 3, "name": "ckimpton2"}}, {"myObject": {"id": 4, "name": "cferran3"}}, {"myObject": {"id": 5, "name": "ililie4"}}, {"myObject": {"id": 6, "name": "wheineke5"}}, {"myObject": {"id": 7, "name": "kasprey6"}}, {"myObject": {"id": 8, "name": "eeuesden7"}}, {"myObject": {"id": 9, "name": "kpridmore8"}}, {"myObject": {"id": 10, "name": "akittiman9"}}]'
    expect = [{'myObject': {'id': 1, 'name': 'wcrissil0'}}, {'myObject': {'id': 2, 'name': 'qabdee1'}}, {'myObject': {'id': 3, 'name': 'ckimpton2'}}, {'myObject': {'id': 4, 'name': 'cferran3'}}, {'myObject': {'id': 5, 'name': 'ililie4'}}, {'myObject': {'id': 6, 'name': 'wheineke5'}}, {'myObject': {'id': 7, 'name': 'kasprey6'}}, {'myObject': {'id': 8, 'name': 'eeuesden7'}}, {'myObject': {'id': 9, 'name': 'kpridmore8'}}, {'myObject': {'id': 10, 'name': 'akittiman9'}}]

    with patch.object(http, "make_request") as mock_make_request:
        with patch.object(requests, "read_response", return_value=mock_return_value) as mock_read:
            # act
            actual = http.GetRequest(get_request_url,
                            http.contentType.json.name,
                            get_request_headers,
                            get_request_query_string,
                            False,
                            False,
                            True)
            # assert
            assert actual == expect

def test_decode_utf_16():
    # setup
    get_request_url = "https://my.test.url.com/endpoint"
    get_request_headers = {"x-api-key": "000b2bb0"}
    get_request_query_string = {"field1": "value1"}
    mock_return_value = b'\xff\xfe[\x00{\x00"\x00m\x00y\x00O\x00b\x00j\x00e\x00c\x00t\x00"\x00:\x00 \x00{\x00"\x00i\x00d\x00"\x00:\x00 \x001\x00,\x00 \x00"\x00n\x00a\x00m\x00e\x00"\x00:\x00 \x00"\x00w\x00c\x00r\x00i\x00s\x00s\x00i\x00l\x000\x00"\x00}\x00}\x00,\x00 \x00{\x00"\x00m\x00y\x00O\x00b\x00j\x00e\x00c\x00t\x00"\x00:\x00 \x00{\x00"\x00i\x00d\x00"\x00:\x00 \x001\x000\x00,\x00 \x00"\x00n\x00a\x00m\x00e\x00"\x00:\x00 \x00"\x00a\x00k\x00i\x00t\x00t\x00i\x00m\x00a\x00n\x009\x00"\x00}\x00}\x00]\x00'
    expect = [{'myObject': {'id': 1, 'name': 'wcrissil0'}}, {'myObject': {'id': 10, 'name': 'akittiman9'}}]

    with patch.object(http, "make_request") as mock_make_request:
        with patch.object(requests, "read_response", return_value=mock_return_value) as mock_read:
            # act
            actual = http.GetRequest(get_request_url,
                            http.contentType.json.name,
                            get_request_headers,
                            get_request_query_string,
                            False,
                            False,
                            True)
            # assert
            assert actual == expect

def test_deprecated_get_request_passes_correct_args_to_make_request():
    # setup
    get_request_url = "https://my.test.url.com/endpoint"
    get_request_headers = {"x-api-key": "000b2bb0"}
    get_request_query_string = {"field1": "value1"}
    expected_host = "my.test.url.com"
    expected_url = "/endpoint?field1=value1"
    expected_payload = None
    expected_headers = {"x-api-key": "000b2bb0", "content-type": "application/json"}
    expected_request_action = "GET"

    with patch.object(http, "make_request") as mock_make_request:

        # act
        http.GetRequest(get_request_url,
                        http.contentType.json.name,
                        get_request_headers,
                        get_request_query_string,
                        False,
                        False,
                        False)
        # assert
        mock_make_request.assert_called_once_with(expected_request_action,
                                                  expected_host,
                                                  expected_url,
                                                  expected_headers,
                                                  expected_payload
                                                  )

def test_get_request_passes_correct_args_to_make_request():
    # setup
    get_request_url = "https://my.test.url.com/endpoint"
    get_request_headers = {"x-api-key": "000b2bb0"}
    get_request_query_string = {"field1": "value1"}
    expected_host = "my.test.url.com"
    expected_url = "/endpoint?field1=value1"
    expected_payload = None
    expected_headers = {"x-api-key": "000b2bb0", "content-type": "application/json"}
    expected_request_action = "GET"

    with patch.object(http, "make_request") as mock_make_request:

    # act
        http.get_request(get_request_url,
                         http.contentType.json.name,
                         get_request_headers,
                         get_request_query_string,
                         False,
                         False,
                         False)
    # assert
        mock_make_request.assert_called_once_with(expected_request_action,
                                                  expected_host,
                                                  expected_url,
                                                  expected_headers,
                                                  expected_payload
                                                  )

def test_form_deprecated_post_request_passes_correct_args_to_make_request_with_basic_auth():
    # setup
    post_request_url = "https://my.test.url.com/endpoint"
    get_request_headers = {"x-api-key": "000b2bb0"}
    expected_host = "my.test.url.com"
    expected_url = "/endpoint"
    expected_payload = "id=3&name=attribute2"
    expected_headers = {"x-api-key": "000b2bb0", "authorization": "Basic bXlJZDpteVNlY3JldA==", "content-type": "application/x-www-form-urlencoded"}
    expected_request_action = "POST"

    http.setBasicAuth('myId', 'mySecret')

    with patch.object(http, "make_request") as mock_make_request:

    # act
        http.PostRequest(post_request_url,
                         http.contentType.xwwwformurlencoded.name,
                         get_request_headers,
                         expected_payload,
                         None,
                         True,
                         False,
                         False)
    # assert
        mock_make_request.assert_called_once_with(expected_request_action,
                                                  expected_host,
                                                  expected_url,
                                                  expected_headers,
                                                  expected_payload
                                                  )

def test_json_deprecated_post_request_passes_correct_args_to_make_request_with_basic_auth():
    # setup
    post_request_url = "https://my.test.url.com/endpoint"
    get_request_headers = {"x-api-key": "000b2bb0"}
    expected_host = "my.test.url.com"
    expected_url = "/endpoint"
    expected_payload = "{\"id\": 3, \"name\": \"attribute2\"}"
    expected_headers = {"x-api-key": "000b2bb0", "authorization": "Basic bXlJZDpteVNlY3JldA==", "content-type": "application/json"}
    expected_request_action = "POST"

    http.setBasicAuth('myId', 'mySecret')

    with patch.object(http, "make_request") as mock_make_request:

    # act
        http.PostRequest(post_request_url,
                         http.contentType.json.name,
                         get_request_headers,
                         None,
                         expected_payload,
                         True,
                         False,
                         False)
    # assert
        mock_make_request.assert_called_once_with(expected_request_action,
                                                  expected_host,
                                                  expected_url,
                                                  expected_headers,
                                                  expected_payload
                                                  )

def test_json_deprecated_post_request_passes_correct_args_to_make_request_with_bearer_token():
    # setup
    post_request_url = "https://my.test.url.com/endpoint"
    get_request_headers = {"x-api-key": "000b2bb0"}
    expected_host = "my.test.url.com"
    expected_url = "/endpoint"
    expected_payload = "{\"id\": 3, \"name\": \"attribute2\"}"
    expected_headers = {"x-api-key": "000b2bb0", "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIi"
                                                                  "OiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0Ijox"
                                                                  "NTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_ad"
                                                                  "Qssw5c",
                        "content-type": "application/json"}
    expected_request_action = "POST"

    http.setBearerToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0I"
                        "joxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c")

    with patch.object(http, "make_request") as mock_make_request:

    # act
        http.PostRequest(post_request_url,
                         http.contentType.json.name,
                         get_request_headers,
                         None,
                         expected_payload,
                         False,
                         True,
                         False)
    # assert
        mock_make_request.assert_called_once_with(expected_request_action,
                                                  expected_host,
                                                  expected_url,
                                                  expected_headers,
                                                  expected_payload
                                                  )

def test_json_post_request_passes_correct_args_to_make_request_with_basic_auth():
    # setup
    post_request_url = "https://my.test.url.com/endpoint"
    get_request_headers = {"x-api-key": "000b2bb0"}
    expected_host = "my.test.url.com"
    expected_url = "/endpoint"
    expected_payload = "{\"id\": 3, \"name\": \"attribute2\"}"
    expected_headers = {"x-api-key": "000b2bb0", "authorization": "Basic bXlJZDpteVNlY3JldA==",
                        "content-type": "application/json"}
    expected_request_action = "POST"

    http.setBasicAuth('myId', 'mySecret')

    with patch.object(http, "make_request") as mock_make_request:

    # act
        http.post_request(post_request_url,
                          http.contentType.json.name,
                          get_request_headers,
                          expected_payload,
                          True,
                          False,
                          False)
    # assert
        mock_make_request.assert_called_once_with(expected_request_action,
                                                  expected_host,
                                                  expected_url,
                                                  expected_headers,
                                                  expected_payload
                                                  )

def test_request_passes_correct_args_to_make_request_with_custom_content_type():
    # setup
    post_request_url = "https://my.test.url.com/endpoint"
    get_request_headers = {"x-api-key": "000b2bb0"}
    expected_host = "my.test.url.com"
    expected_url = "/endpoint"
    expected_payload = "{\"id\": 3, \"name\": \"attribute2\"}"
    expected_headers = {"x-api-key": "000b2bb0",
                        "content-type": "custom_type"}
    expected_request_action = "POST"

    with patch.object(http, "make_request") as mock_make_request:

    # act
        http.post_request(post_request_url,
                          'custom_type',
                          get_request_headers,
                          expected_payload,
                          False,
                          False,
                          False)
    # assert
        mock_make_request.assert_called_once_with(expected_request_action,
                                                  expected_host,
                                                  expected_url,
                                                  expected_headers,
                                                  expected_payload
                                                  )

def test_json_post_request_passes_correct_args_to_make_request_with_no_headers():
    # setup
    post_request_url = "https://my.test.url.com/endpoint"
    get_request_headers = {"x-api-key": "000b2bb0"}
    expected_host = "my.test.url.com"
    expected_url = "/endpoint"
    expected_payload = "{\"id\": 3, \"name\": \"attribute2\"}"
    expected_headers = {}
    expected_request_action = "POST"

    # http.setBasicAuth('myId', 'mySecret')

    with patch.object(http, "make_request") as mock_make_request:

    # act
        http.post_request(post_request_url,
                          None,
                          None,
                          expected_payload,
                          False,
                          False,
                          False)
    # assert
        mock_make_request.assert_called_once_with(expected_request_action,
                                                  expected_host,
                                                  expected_url,
                                                  expected_headers,
                                                  expected_payload
                                                  )

def test_json_post_request_passes_correct_args_to_make_request_with_bearer_token():
    # setup
    post_request_url = "https://my.test.url.com/endpoint"
    get_request_headers = {"x-api-key": "000b2bb0"}
    expected_host = "my.test.url.com"
    expected_url = "/endpoint"
    expected_payload = "{\"id\": 3, \"name\": \"attribute2\"}"
    expected_headers = {"x-api-key": "000b2bb0", "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIi"
                                                                  "OiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0Ijox"
                                                                  "NTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_ad"
                                                                  "Qssw5c",
                        "content-type": "application/json"}
    expected_request_action = "POST"

    http.setBearerToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0I"
                        "joxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c")

    with patch.object(http, "make_request") as mock_make_request:

    # act
        http.post_request(post_request_url,
                          http.contentType.json.name,
                          get_request_headers,
                          expected_payload,
                          False,
                          True,
                          False)
    # assert
        mock_make_request.assert_called_once_with(expected_request_action,
                                                  expected_host,
                                                  expected_url,
                                                  expected_headers,
                                                  expected_payload
                                                  )


