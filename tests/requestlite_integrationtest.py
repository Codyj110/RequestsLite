import unittest
import os
from requestslite.requests import requests
http = requests()

class testReqeust(unittest.TestCase):
    """4 test 3 with no auth just a get 2 post one with form one with json
    the 4th test is testing getting a token from an oauth service.
    For my test I used mockaroo for the no auth and bitbucket for the auth"""
    http = requests()
    id = "*********"
    secret = "************"
    noAuthGetPostUrl = os.environ['inturl']
    OAuthUrl = 'https://'
    APIKey = os.environ['APIKey']

    def test_getRequest(self):

        headers = {'x-api-key': self.APIKey}

        actual = self.http.GetRequest(self.noAuthGetPostUrl,
                                        self.http.contentType.json.name,
                                        headers,
                                        None,
                                        False,
                                        False,
                                        False)
        expected = 200
        self.assertEqual(actual.code, expected)

    def test_get_request(self):

        headers = {'x-api-key': self.APIKey}

        actual = self.http.get_request(self.noAuthGetPostUrl,
                                       self.http.contentType.json.name,
                                       headers,
                                       None,
                                       False,
                                       False,
                                       False)
        expected = 200
        self.assertEqual(actual.code, expected)

    def test_successful_deprecated_post_request_with_json_data(self):
        # setup
        post_data = "{\"id\": 3, \"name\": \"attribute2\"}"
        headers = {'x-api-key': self.APIKey}
        expected_return_id = 3
        expected_response_code = 200

        # act
        response = self.http.PostRequest(self.noAuthGetPostUrl,
                                         self.http.contentType.json.name,
                                         headers,
                                         None,
                                         post_data,
                                         False,
                                         False,
                                         False)

        actual_response_code = response.code
        actual_return_id = self.http.decodeParse(response, True)['id']

        # assert
        # got a good 200 response
        assert actual_response_code == expected_response_code

        # did we get the data we passed in
        assert actual_return_id == expected_return_id

    def test_succesful_deprecated_post_request_with_form(self):
        # setup
        post_data = "id=3&name=attribute2"
        headers = {'x-api-key': self.APIKey}
        expected_response_code = 200
        expected_return_id = '3'

        #act
        response = self.http.PostRequest(self.noAuthGetPostUrl,
                                         self.http.contentType.xwwwformurlencoded.name,
                                         headers,
                                         post_data,
                                         None,
                                         False,
                                         False,
                                         False)

        actual_response_code = response.code
        actual_return_id = self.http.decodeParse(response, True)['id']

        # assert
        # got a good 200 response
        assert actual_response_code == expected_response_code

        # did we get the data we passed in
        assert actual_return_id == expected_return_id

    def test_successful_post_request_with_json_data(self):
        # setup
        expected_payload = "{\"id\": 3, \"name\": \"attribute2\"}"
        headers = {'x-api-key': self.APIKey}
        expected_return_id = 3
        expected_response_code = 200

        # act
        response = self.http.post_request(self.noAuthGetPostUrl,
                                          self.http.contentType.json.name,
                                          headers,
                                          expected_payload,
                                          False,
                                          False,
                                          False)

        actual_response_code = response.code
        actual_return_id = self.http.decodeParse(response, True)['id']

        # assert
        # got a good 200 response
        assert actual_response_code == expected_response_code

        # did we get the data we passed in
        assert actual_return_id == expected_return_id

    def test_succesful_post_request_with_form(self):
        # setup
        expected_payload = "id=3&name=attribute2"
        headers = {'x-api-key': self.APIKey}
        expected_response_code = 200
        expected_return_id = '3'

        #act
        response = self.http.post_request(self.noAuthGetPostUrl,
                                          self.http.contentType.xwwwformurlencoded.name,
                                          headers,
                                          expected_payload,
                                          False,
                                          False,
                                          False)

        actual_response_code = response.code
        actual_return_id = self.http.decodeParse(response, True)['id']

        # assert
        # got a good 200 response
        assert actual_response_code == expected_response_code

        # did we get the data we passed in
        assert actual_return_id == expected_return_id

    def xtest_postOAuth(self):
        id = self.id
        secret = self.secret
        self.http.setBasicAuth(id,secret)
        response = self.http.PostRequest(self.OAuthUrl,
                                         self.http.contentType.xwwwformurlencoded.name,
                                         None,
                                         "grant_type=client_credentials",
                                         None,
                                         True,
                                         False,
                                         False)
        self.assertEqual(response.code,200)

if __name__ == '__main__':
    unittest.main()